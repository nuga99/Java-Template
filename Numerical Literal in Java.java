import java.io.*;
import java.util.Scanner;

class MainProgram{
    
  public static void main(String [] args){
    
    /* Numerical Literal Java makes more comfortable and viewable */
    
    double Angka = 2.800_999_1;
    System.out.println(Angka);
    
    int d = 2_000_000;
    System.out.println(d);
  }
}