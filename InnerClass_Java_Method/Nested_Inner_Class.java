
/*
1. Nested Class

    Nested Inner class can access any private instance variable of outer class. Like any other instance variable, we can have access modifier private, protected, public and default modifier.
Like class, interface can also be nested and can have access specifiers. 

*/

class Outer{ //Outer Class
    
    class Inner{ //Inner Class
        
        public void show(){
            
            System.out.println("Manual Testing using Nested Inner Class.");
        }
    }
}

// Save the name file like Class , i.e "Main"

class Main{
    
    public static void main(String [] args){
        
        Outer.Inner NestedClass = new Outer().new Inner();
        NestedClass.show(); //Invoke the "show" method
    }
}